from typing import Union
from bot.models import Exchange
from os.path import join, dirname
from dotenv import load_dotenv
import pybitflyer
import python_bitbankcc
from zaifapi import *
# from coincheck import market
from bot.adapters.bitflyer import Bitflyer
from bot.adapters.bitbank import Bitbank
from bot.adapters.zaif import Zaif
from bot.adapters.coincheck import Coincheck
from bot.adapters.btcbox import Btcbox
from bot.adapters.poloniex_wrapper import PoloniexWrapper
from bot.adapters.bittrex_wrapper import BittrexWrapper
from bot.adapters.binance import Binance
from bot.adapters.exchange_adapter import ExchangeAdapter
from forex_python.converter import CurrencyRates
from btcbox_client.sync import Client
from django.test import TestCase
from poloniex import Poloniex
from bittrex.bittrex import Bittrex, API_V2_0, API_V1_1
import os
import pprint
from unittest import skipIf
from quoine.client import Quoinex
from coincheck.coincheck import CoinCheck
import logging
logger = logging.getLogger('django')
dotenv_path: Union[bytes, str] = join(dirname(__file__), '../../.env')
load_dotenv(dotenv_path)


# class BitbankTests(TestCase):
#     def test_ticker(self):
#         api = python_bitbankcc.public()
#         value = api.get_ticker('btc_jpy')
#         self.assertTrue('sell' in value.keys())
#         self.assertTrue('buy' in value.keys())
#
#     def test_save(self):
#         api = python_bitbankcc.public()
#         value = api.get_ticker('btc_jpy')
#         exchange = Exchange()
#         exchange.name = Exchange.BB
#         exchange.btc_jpy_bid = value['sell']
#         exchange.btc_jpy_ask = value['buy']
#         exchange.save()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_collect(self):
#         api = Bitbank()
#         api.collect()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_balance(self):
#         api = python_bitbankcc.private(os.environ.get('BITBANK_KEY'), os.environ.get('BITBANK_SECRET'))
#         balance = api.get_asset()
#         self.assertEqual(balance['assets'][0]['asset'], 'jpy')
#         self.assertEqual(float(balance['assets'][0]['free_amount']), 0)
#         self.assertEqual(balance['assets'][1]['asset'], 'btc')
#         self.assertEqual(float(balance['assets'][1]['free_amount']), 0)
#         self.assertEqual(float(balance['assets'][1]['withdrawal_fee']), 0.001)
# #
#     def test_withdraw(self):
#         Bitbank().collect()
#         Bitbank().withdraw(Zaif.ADDRESS)
#         self.assertRaises(Exception, exchange.withdraw, 'e9fb5d9f-0509-4cb5-8325-ec13ade4354c')
#
#     def test_trade_ask(self):
#         Bitbank().collect()
#         Bitbank().trade_ask()
#         self.assertRaises(Exception, exchange.trade_ask)
#
#     def test_trade_bid(self):
#         Bitbank().collect()
#         Bitbank().trade_bid()
#         self.assertRaises(Exception, exchange.trade_bid)
#
#     def test_measure_request_amount(self):
#         TaskTests.save_ticker()
#         exchange = Bitbank()
#         exchange.measure_request_amount()
#         self.assertTrue(exchange.jpy == 0)
#         self.assertTrue(exchange.price != 0)
#         self.assertTrue(exchange.amount == 0)

class ZaifTests(TestCase):
    def test_ticker(self):
        zaif = ZaifPublicApi()
        response = zaif.last_price('btc_jpy')
        self.assertTrue('last_price' in response.keys())
#
#     def test_save(self):
#         zaif = ZaifPublicApi()
#         response = zaif.last_price('btc_jpy')
#         exchange = Exchange()
#         exchange.name = Exchange.Z
#         exchange.btc_jpy_bid = response['last_price']
#         exchange.btc_jpy_ask = response['last_price']
#         exchange.save()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_collect(self):
#         api = Zaif()
#         api.collect()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_balance(self):
#         jpy, btc = Zaif().get_balance()
#         self.assertEqual(jpy, 0)
#         self.assertEqual(btc, 0)
#
# def test_withdraw(self):
#     TaskTests.save_ticker()
#     exchange = Zaif()
#     exchange.withdraw(Bitbank.ADDRESS)
# self.assertRaises(Exception, exchange.withdraw, Bitbank.ADDRESS)
#
#     def test_trade_ask(self):
#         Zaif().collect()
#         exchange = Zaif()
#         exchange.trade_ask()
#         self.assertTrue(exchange.jpy > 0)
#
#     def test_trade_bid(self):
#         Zaif().collect()
#         exchange = Zaif()
#         exchange.trade_bid()
#         self.assertTrue(exchange.jpy > 0)
#
#     def test_measure_request_amount(self):
#         TaskTests.save_ticker()
#         exchange = Zaif()
#         exchange.measure_request_amount()
#         self.assertTrue(exchange.jpy == 0)
#         self.assertTrue(exchange.price != 0)
#         self.assertTrue(exchange.amount == 0)
#
# class CoincheckTests(TestCase):
#     def test_ticker(self):
#         coincheck = CoinCheck('ACCESS_KEY', 'API_SECRET')
#         response = coincheck.ticker.all()
#         self.assertTrue('bid' in response.keys())
#         self.assertTrue('ask' in response.keys())
#
#     def test_save(self):
#         coincheck = CoinCheck('ACCESS_KEY', 'API_SECRET')
#         response = coincheck.ticker.all()
#         exchange = Exchange()
#         exchange.name = Exchange.CC
#         exchange.btc_jpy_bid = response['bid']
#         exchange.btc_jpy_ask = response['ask']
#         exchange.save()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_collect(self):
#         api = Coincheck()
#         api.collect()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_balance(self):
#         jpy, btc = Coincheck().get_balance()
#         self.assertEqual(jpy, 0.7)
#         self.assertEqual(btc, 0)
#
#     def test_withdraw(self):
#         TaskTests.save_ticker()
#         exchange = Coincheck()
#         self.assertRaises(Exception, exchange.withdraw, 'e9fb5d9f-0509-4cb5-8325-ec13ade4354c')
#
#     def test_trade_ask(self):
#         TaskTests.save_ticker()
#         exchange = Coincheck()
#         exchange.measure_request_amount()
#         self.assertRaises(Exception, exchange.trade_ask)
#
#     def test_trade_bid(self):
#         TaskTests.save_ticker()
#         exchange = Coincheck()
#         exchange.measure_request_amount()
#         self.assertRaises(Exception, exchange.trade_bid)
#
#     def test_measure_request_amount(self):
#         TaskTests.save_ticker()
#         exchange = Coincheck()
#         exchange.measure_request_amount()
#         self.assertTrue(exchange.jpy == 0)
#         self.assertTrue(exchange.price != 0)
#         self.assertTrue(exchange.amount == 0)

#
# class BtcboxTests(TestCase):
#     def test_ticker(self):
#         client = Client(
#             public_key=os.environ.get('BTCBOX_KEY'), private_key=os.environ.get('BTCBOX_SECRET'))
#         response = client.ticker()
#         self.assertTrue('buy' in response.json().keys())
#         self.assertTrue('sell' in response.json().keys())
#
#     def test_save(self):
#         client = Client(
#             public_key=os.environ.get('BTCBOX_KEY'), private_key=os.environ.get('BTCBOX_SECRET'))
#         response = client.ticker()
#         exchange = Exchange()
#         exchange.name = Exchange.BF
#         exchange.btc_jpy_bid = response.json()['sell']
#         exchange.btc_jpy_ask = response.json()['buy']
#         exchange.save()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_collect(self):
#         api = Btcbox()
#         api.collect()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_balance(self):
#         api = Client(public_key=os.environ.get('BTCBOX_KEY'), private_key=os.environ.get('BTCBOX_SECRET'))
#         response = api.balance()
#         pprint.pprint(response.json())
#         # self.assertEqual(balance['deposit']['jpy'], 0)
#         # self.assertEqual(balance['deposit']['btc'], 0)

# class BitflyerTests(TestCase):
#     def test_ticker(self):
#         api = pybitflyer.API(
#             api_key=os.environ.get("BITFLYER_KEY"),
#             api_secret=os.environ.get("BITFLYER_SECRET"))
#         ticker = api.ticker(product_code="BTC_JPY")
#         self.assertTrue('best_ask' in ticker.keys())
#         self.assertTrue('best_bid' in ticker.keys())
#
#     def test_save(self):
#         api = pybitflyer.API(
#             api_key=os.environ.get("BITFLYER_KEY"),
#             api_secret=os.environ.get("BITFLYER_SECRET"))
#         ticker = api.ticker(product_code="BTC_JPY")
#         exchange = Exchange()
#         exchange.name = Exchange.BF
#         exchange.btc_jpy_bid = ticker['best_bid']
#         exchange.btc_jpy_ask = ticker['best_ask']
#         currency = CurrencyRates()
#         exchange.btc_usd_bid = currency.convert('JPY', 'USD',
#                                                 ticker['best_bid'])
#         exchange.btc_usd_ask = currency.convert('JPY', 'USD',
#                                                 ticker['best_ask'])
#         exchange.save()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_collect(self):
#         api = Bitflyer()
#         api.collect()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_balance(self):
#         api = pybitflyer.API(
#                             api_key=os.environ.get("BITFLYER_KEY"),
#                             api_secret=os.environ.get("BITFLYER_SECRET"))
#         balance = api.getbalance()
#         self.assertEqual(balance[0]['currency_code'], 'JPY')
#         self.assertEqual(float(balance[0]['amount']), 14691.0)
#         self.assertEqual(balance[1]['currency_code'], 'BTC')
#         self.assertEqual(float(balance[1]['amount']), 0.079)
#
# class PoloniexTests(TestCase):
#     def test_ticker(self):
#         polo = Poloniex()
#         response = polo('returnTicker')['USDT_BTC']
#         self.assertTrue('lowestAsk' in response.keys())
#         self.assertTrue('highestBid' in response.keys())
#
#     def test_save(self):
#         polo = Poloniex()
#         response = polo('returnTicker')['USDT_BTC']
#         exchange = Exchange()
#         exchange.name = Exchange.PN
#         exchange.btc_usd_bid = response['highestBid']
#         exchange.btc_usd_ask = response['lowestAsk']
#         exchange.save()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_collect(self):
#         api = PoloniexWrapper()
#         api.collect()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)

# class BittrexTests(TestCase):
#     def test_ticker(self):
#         bittrex = Bittrex(None, None, api_version=API_V1_1)
#         response = bittrex.get_ticker('USD-BTC')['result']
#         self.assertTrue('Bid' in response.keys())
#         self.assertTrue('Ask' in response.keys())
#
#     def test_save(self):
#         bittrex = Bittrex(None, None, api_version=API_V1_1)
#         response = bittrex.get_ticker('USD-BTC')['result']
#         exchange = Exchange()
#         exchange.name = Exchange.BT
#         exchange.btc_usd_bid = response['Bid']
#         exchange.btc_usd_ask = response['Ask']
#         exchange.save()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_collect(self):
#         api = BittrexWrapper()
#         api.collect()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)

#
# class BinanceTests(TestCase):
#     def test_ticker(self):
#         binance = Client(None, None)
#         response = binance.get_symbol_ticker(symbol='BTCUSDT')
#         self.assertTrue('price' in response.keys())
#
#     def test_save(self):
#         binance = Client(None, None)
#         response = binance.get_symbol_ticker(symbol='BTCUSDT')
#         exchange = Exchange()
#         exchange.name = Exchange.B
#         exchange.btc_usd_bid = response['price']
#         exchange.btc_usd_ask = response['price']
#         exchange.save()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
#     def test_collect(self):
#         api = Binance()
#         api.collect()
#         saved_exchanges = Exchange.objects.all()
#         self.assertEqual(saved_exchanges.count(), 1)
#
# class QuoinexTests(TestCase):
#     def test_ticker(self):
#         api = Quoinex(os.environ.get('QUOINEX_KEY'), os.environ.get('QUOINEX_SECRET'))
#         response = api.get_products()
#         btc = [x for x in response if x['base_currency'] == 'BTC' and x['currency'] == 'JPY'][0]
#         self.assertTrue('high_market_ask' in btc.keys())
#         self.assertTrue('low_market_bid' in btc.keys())
#
#     def test_balance(self):
#         api = Quoinex(os.environ.get('QUOINEX_KEY'), os.environ.get('QUOINEX_SECRET'))
#         response = api.get_account_balances()
#         btc = [x for x in response if x['currency'] == 'BTC'][0]
#         jpy = [x for x in response if x['currency'] == 'JPY'][0]
#         self.assertTrue('balance' in btc.keys())
#         self.assertTrue('balance' in jpy.keys())
