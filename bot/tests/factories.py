from bot.models import Exchange
import factory


class ExchangeFactory(factory.django.DjangoModelFactory):
    class Meta(object):
        model = Exchange
