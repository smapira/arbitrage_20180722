from __future__ import absolute_import, unicode_literals
from celery import Celery
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'arbitrage.settings')
import django
django.setup()
from bot.settlement_states.register_machine import RegisterMachine
from bot.settlement_states.banker import Banker
import sys, traceback
import logging
logger = logging.getLogger('django')

app = Celery('tasks', broker='sqla+sqlite:///celerydb.sqlite')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

COUNT_SEC = 60
COUNT_UP = 30
RE_COUNT_UP = 1
APPEND_SETTLE = 'Enqueue settle'
APPEND_RELEASE = 'Enqueue release'


@app.task
def collect():
    """Collect ticker"""
    try:
        banker = Banker()
        RegisterMachine(banker, 'banker')
        banker.run()
    except Exception as e:
        logger.error('-' * 60)
        logger.error(e)
        traceback.print_exc(file=sys.stdout)
        logger.error('-' * 60)
    finally:
        collect.apply_async(countdown=COUNT_SEC)
