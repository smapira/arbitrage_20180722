from bot.adapters.bitflyer import Bitflyer
from bot.adapters.bitbank import Bitbank
from bot.adapters.exchange_adapter import ExchangeAdapter
from bot.adapters.zaif import Zaif
from bot.adapters.coincheck import Coincheck
from bot.adapters.btcbox import Btcbox
from bot.adapters.binance import Binance
from bot.adapters.bittrex_wrapper import BittrexWrapper
from bot.adapters.poloniex_wrapper import PoloniexWrapper
from bot.models import Exchange
from bot.jobs.ruling import Ruling
from pytz import timezone
import logging
logger = logging.getLogger('django')


class Banker:
    PERIOD = 240

    def __init__(self):
        Bitflyer().collect()
        Zaif().collect()
        Bitbank().collect()
        Coincheck().collect()
        Btcbox().collect()
        PoloniexWrapper().collect()
        BittrexWrapper().collect()
        Binance().collect()

    def unregister(self):
        pass

    def register(self):
        pass

    def remit(self):
        pass

    def clear(self):
        pass
