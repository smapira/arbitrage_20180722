from transitions import Machine, State


class RegisterMachine(object):

    states = [
        State(name='unregister', on_exit=['unregister']),
        State(name='register', on_enter=['register']),
        State(name='remit', on_enter=['remit']),
        State(name='clear', on_enter=['clear'])
    ]
    transitions = [
        {
            'trigger': 'run',
            'source': 'unregister',
            'dest': 'register',
        },
        {
            'trigger': 'run',
            'source': 'register',
            'dest': 'remit',
        },
        {
            'trigger': 'run',
            'source': 'remit',
            'dest': 'clear',
        },
    ]

    def __init__(self, model, name):
        self.name = name
        self.kittens_rescued = 0
        self.machine = Machine(
            model,
            transitions=RegisterMachine.transitions,
            states=RegisterMachine.states,
            initial='unregister')
