from decimal import Decimal, ROUND_FLOOR
from bot.models import Exchange
import logging
logger = logging.getLogger('django')


class ApiAdaptee:
    def __init__(self):
        self.jpy = 0
        self.btc = 0
        self.price = 0
        self.amount = 0

    def collect(self):
        raise NotImplementedError

    def save(self, response: any):
        raise NotImplementedError

    def get_balance(self) -> tuple:
        raise NotImplementedError

    def withdraw(self, address: str):
        raise NotImplementedError

    def trade_ask(self):
        raise NotImplementedError

    def trade_bid(self):
        raise NotImplementedError

    def get_address(self) -> str:
        raise NotImplementedError

    def measure_request_amount(self):
        raise NotImplementedError

    def measure_amount(self, exchange):
        logger.info('ApiAdaptee.exchange: ' + str(exchange))
        rates = Exchange.objects.all().filter(name=exchange).values(
            'btc_jpy_ask', 'btc_jpy_bid')[:1].first()
        if rates is None:
            return
        if self.btc > 0.0001:
            self.price = Decimal(rates['btc_jpy_ask'])
            if self.price == 0:
                return
            self.amount = Decimal(self.btc).quantize(Decimal('0.0001'))
        else:
            self.price = Decimal(rates['btc_jpy_bid'])
            if self.price == 0:
                return
            self.amount = (Decimal(self.jpy) / Decimal(self.price)).quantize(
                Decimal('0.0001'), rounding=ROUND_FLOOR)
        logger.info('ApiAdaptee.jpy: ' + str(self.jpy))
        logger.info('ApiAdaptee.price: ' + str(self.price))
        logger.info('ApiAdaptee.amount: ' + str(self.amount))
        logger.info('ApiAdaptee.calculate: ' + str(self.price * self.amount))
