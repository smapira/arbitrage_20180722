from bot.models import Exchange
from bot.adapters.api_adaptee import ApiAdaptee
import python_bitbankcc
from forex_python.converter import CurrencyRates
from decimal import *
import os
"""Bitbank API Wrapper.

This module handling API.

.. _API Guide:
   https://docs.bitbank.cc
"""


class Bitbank(ApiAdaptee):
    WITHDRAW_FEE = Decimal(0.001)
    ADDRESS = '3FRxc84WdhA1HmeENViBGbbLMP4UV7YvWn'
    """Bitbank Exchange class.

    Attributes:
        api (object): python_bitbankcc.public()
        api_private (object): python_bitbankcc.private()

    """

    def __init__(self):
        super().__init__()
        self.api = python_bitbankcc.public()
        self.api_private = python_bitbankcc.private(
            os.environ.get('BITBANK_KEY'), os.environ.get('BITBANK_SECRET'))
        self.get_balance()

    def get_address(self) -> str:
        return self.ADDRESS

    def measure_request_amount(self):
        self.measure_amount(Exchange.BB)

    def collect(self):
        response = self.api.get_ticker('btc_jpy')
        self.save(response)

    def save(self, response):
        exchange = Exchange()
        exchange.name = Exchange.BB
        exchange.btc_jpy_bid = response['sell']
        exchange.btc_jpy_ask = response['buy']
        currency = CurrencyRates()
        exchange.btc_usd_bid = currency.convert('JPY', 'USD',
                                                Decimal(response['sell']))
        exchange.btc_usd_ask = currency.convert('JPY', 'USD',
                                                Decimal(response['buy']))
        exchange.save()

    def get_balance(self) -> tuple:
        if os.environ.get('BITBANK_KEY') is None:
            return None, None
        balance = self.api_private.get_asset()
        self.jpy = float(balance['assets'][0]['free_amount'])
        self.btc = float(balance['assets'][1]['free_amount'])
        self.measure_request_amount()
        return self.jpy, self.btc

    def trade_ask(self):
        response = self.api_private.order('btc_jpy', int(self.price),
                                          str(self.amount), 'sell', 'limit')
        self.price = response['price']
        self.amount = response['remaining_amount']

    def trade_bid(self):
        response = self.api_private.order('btc_jpy', int(self.price),
                                          str(self.amount), 'buy', 'limit')
        self.price = response['price']
        self.amount = response['remaining_amount']

    def withdraw(self, address: str):
        value = self.api_private.get_withdraw_account('btc')
        uuid = [x for x in value['accounts']
                if x['address'] == address][0]['uuid']
        self.amount = (self.amount - self.WITHDRAW_FEE).quantize(
            Decimal('0.0001'))
        self.api_private.request_withdraw('btc', uuid, str(self.amount), {})
