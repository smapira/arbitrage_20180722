from bot.models import Exchange
from bot.adapters.api_adaptee import ApiAdaptee
from zaifapi import *
from decimal import Decimal, ROUND_FLOOR
import os
from forex_python.converter import CurrencyRates
import logging
logger = logging.getLogger('django')
"""Zaif API Wrapper.

This module handling API.

.. _API Guide:
   http://techbureau-api-document.readthedocs.io/ja/latest/index.html
"""


class Zaif(ApiAdaptee):
    WITHDRAW_FEE = Decimal(0.0005)
    ADDRESS = '3Ftb8CL8ArPVNFKYdo9WX81WBYgqHtV14e'
    """Zaif Exchange class.

    Attributes:
        api (object): ZaifPublicApi()
        api_private (object): ZaifTradeApi()

    """

    def __init__(self):
        super().__init__()
        self.api = ZaifPublicApi()
        self.api_private = ZaifTradeApi(
            os.environ.get('ZAIF_KEY'), os.environ.get('ZAIF_SECRET'))
        self.get_balance()

    def get_address(self) -> str:
        return self.ADDRESS

    def collect(self):
        response = self.api.last_price('btc_jpy')
        self.save(response)

    def save(self, response):
        exchange = Exchange()
        exchange.name = Exchange.Z
        exchange.btc_jpy_bid = response['last_price']
        exchange.btc_jpy_ask = response['last_price']
        currency = CurrencyRates()
        exchange.btc_usd_bid = currency.convert('JPY', 'USD',
                                                response['last_price'])
        exchange.btc_usd_ask = currency.convert('JPY', 'USD',
                                                response['last_price'])
        exchange.save()

    def get_balance(self) -> tuple:
        if os.environ.get('ZAIF_KEY') is None:
            return None, None
        balance = self.api_private.get_info()
        self.jpy = float(balance['deposit']['jpy'])
        self.btc = float(balance['deposit']['btc'])
        self.measure_request_amount()
        return self.jpy, self.btc

    def withdraw(self, address: str):
        self.amount = (Decimal(self.amount) - self.WITHDRAW_FEE).quantize(
            Decimal('0.0001'), rounding=ROUND_FLOOR)
        logger.info(address)
        logger.info(self.amount)
        api_private = ZaifTradeApi(
            os.environ.get('ZAIF_KEY'), os.environ.get('ZAIF_SECRET'))
        api_private.withdraw(currency='btc', address=address, amount=0.0001)
        # self.api_private.withdraw(
        #     currency='btc', address=address, amount=self.amount)

    def trade_ask(self):
        """request trade for ask

        .. _API Guide:
           http://techbureau-api-document.readthedocs.io/ja/latest/public/2_individual/2_currency_pairs.html
           http://techbureau-api-document.readthedocs.io/ja/latest/trade/2_individual/7_trade.html
        """
        self.api_private.trade(
            currency_pair='btc_jpy',
            action='ask',
            price=int(self.price),
            amount=self.amount)

    def trade_bid(self):
        """request trade for bid

        .. _API Guide:
           http://techbureau-api-document.readthedocs.io/ja/latest/public/2_individual/2_currency_pairs.html
           http://techbureau-api-document.readthedocs.io/ja/latest/trade/2_individual/7_trade.html
        """
        self.api_private.trade(
            currency_pair='btc_jpy',
            action='bid',
            price=int(self.price),
            amount=self.amount)

    def measure_request_amount(self):
        self.measure_amount(Exchange.Z)
