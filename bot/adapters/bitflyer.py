from decimal import Decimal

import pybitflyer
from bot.models import Exchange
import os
from forex_python.converter import CurrencyRates
from bot.adapters.api_adaptee import ApiAdaptee


class Bitflyer(ApiAdaptee):
    WITHDRAW_FEE = Decimal(0.0004)

    def get_address(self) -> str:
        pass

    def measure_request_amount(self):
        pass

    def get_balance(self) -> tuple:
        pass

    def withdraw(self, address: str):
        pass

    def trade_ask(self):
        pass

    def trade_bid(self):
        pass

    def collect(self):
        response = self.api.ticker(product_code="BTC_JPY")
        self.save(response)

    def save(self, response):
        exchange = Exchange()
        exchange.name = Exchange.BF
        exchange.btc_jpy_bid = response['best_bid']
        exchange.btc_jpy_ask = response['best_ask']
        currency = CurrencyRates()
        exchange.btc_usd_bid = currency.convert('JPY', 'USD',
                                                response['best_bid'])
        exchange.btc_usd_ask = currency.convert('JPY', 'USD',
                                                response['best_ask'])

        exchange.save()

    def __init__(self):
        super().__init__()
        self.api = pybitflyer.API(
            api_key=os.environ.get("BITFLYER_API_KEY"),
            api_secret=os.environ.get("BITFLYER_API_SECRET"))
