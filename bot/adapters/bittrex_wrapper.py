from bot.models import Exchange
from bittrex.bittrex import Bittrex, API_V2_0, API_V1_1
from forex_python.converter import CurrencyRates
from decimal import Decimal
from bot.adapters.api_adaptee import ApiAdaptee


class BittrexWrapper(ApiAdaptee):
    def get_address(self) -> str:
        pass

    def measure_request_amount(self):
        pass

    def get_balance(self) -> tuple:
        pass

    def withdraw(self, address: str):
        pass

    def trade_ask(self):
        pass

    def trade_bid(self):
        pass

    def collect(self):
        response = self.api.get_ticker('USD-BTC')['result']
        self.save(response)

    def save(self, response):
        exchange = Exchange()
        exchange.name = Exchange.BT
        exchange.btc_usd_bid = response['Bid']
        exchange.btc_usd_ask = response['Ask']
        exchange.btc_jpy_bid = self.currency.convert('USD', 'JPY',
                                                     Decimal(response['Bid']))
        exchange.btc_jpy_ask = self.currency.convert('USD', 'JPY',
                                                     Decimal(response['Ask']))

        exchange.save()

    def __init__(self):
        super().__init__()
        self.api = Bittrex(None, None, api_version=API_V1_1)
        self.currency = CurrencyRates()
