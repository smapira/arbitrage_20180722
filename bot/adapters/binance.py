from bot.models import Exchange
from binance.client import Client
from forex_python.converter import CurrencyRates
from decimal import Decimal
from bot.adapters.api_adaptee import ApiAdaptee


class Binance(ApiAdaptee):
    def get_address(self) -> str:
        pass

    def measure_request_amount(self):
        pass

    def get_balance(self) -> tuple:
        pass

    def withdraw(self, address: str):
        pass

    def trade_ask(self):
        pass

    def trade_bid(self):
        pass

    def collect(self):
        response = self.api.get_symbol_ticker(symbol='BTCUSDT')
        self.save(response)

    def save(self, response):
        exchange = Exchange()
        exchange.name = Exchange.B
        exchange.btc_usd_bid = response['price']
        exchange.btc_usd_ask = response['price']
        exchange.btc_jpy_bid = self.currency.convert(
            'USD', 'JPY', Decimal(response['price']))
        exchange.btc_jpy_ask = self.currency.convert(
            'USD', 'JPY', Decimal(response['price']))

        exchange.save()

    def __init__(self):
        super().__init__()
        self.api = Client(None, None)
        self.currency = CurrencyRates()
