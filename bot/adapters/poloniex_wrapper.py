from bot.models import Exchange
from poloniex import Poloniex
from forex_python.converter import CurrencyRates
from decimal import Decimal
from bot.adapters.api_adaptee import ApiAdaptee


class PoloniexWrapper(ApiAdaptee):
    def get_address(self) -> str:
        pass

    def measure_request_amount(self):
        pass

    def get_balance(self) -> tuple:
        pass

    def withdraw(self, address: str):
        pass

    def trade_ask(self):
        pass

    def trade_bid(self):
        pass

    def collect(self):
        response = self.api('returnTicker')['USDT_BTC']
        self.save(response)

    def save(self, response):
        exchange = Exchange()
        exchange.name = Exchange.PN
        exchange.btc_usd_bid = response['highestBid']
        exchange.btc_usd_ask = response['lowestAsk']
        exchange.btc_jpy_bid = self.currency.convert(
            'USD', 'JPY', Decimal(response['highestBid']))
        exchange.btc_jpy_ask = self.currency.convert(
            'USD', 'JPY', Decimal(response['lowestAsk']))

        exchange.save()

    def __init__(self):
        super().__init__()
        self.api = Poloniex()
        self.currency = CurrencyRates()
