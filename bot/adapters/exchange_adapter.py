from bot.models import Exchange
from bot.adapters.bitflyer import Bitflyer
from bot.adapters.bitbank import Bitbank
from bot.adapters.zaif import Zaif
from bot.adapters.coincheck import Coincheck
from bot.adapters.btcbox import Btcbox
from bot.adapters.poloniex_wrapper import PoloniexWrapper
from bot.adapters.bittrex_wrapper import BittrexWrapper
from bot.adapters.binance import Binance
from bot.adapters.quoinex import Quoinex
import numpy as numpy
from decimal import Decimal
import logging
logger = logging.getLogger('django')


class ExchangeAdapter:
    EXCHANGES = {
        Exchange.BF: 'Bitflyer',
        Exchange.BB: 'Bitbank',
        Exchange.BT: 'BittrexWrapper',
        Exchange.B: 'Binance',
        Exchange.Z: 'Zaif',
        Exchange.CC: 'Coincheck',
        Exchange.BX: 'Btcbox',
        Exchange.QX: 'Quoinex',
        Exchange.PN: 'PoloniexWrapper'
    }
    SPECIMEN_NUMBER = 30

    def __init__(self, _exchange: str):
        self.instance = eval(self.EXCHANGES[_exchange])()

    def get_address(self):
        self.instance.get_address()

    def purchase(self):
        logger.info('ExchangeAdapter.purchase')
        self.instance.trade_bid()

    def sale(self):
        logger.info('ExchangeAdapter.sale')
        self.instance.trade_ask()

    def withdraw(self, address: str):
        logger.info('ExchangeAdapter.withdraw')
        self.instance.withdraw(address)

    def get_balance(self) -> tuple:
        self.instance.get_balance()

    @staticmethod
    def map_moving_average():
        bf = Exchange.objects.all().filter(name=Exchange.BF).values(
            'btc_jpy_bid', 'btc_jpy_ask')[:ExchangeAdapter.SPECIMEN_NUMBER]
        bb = Exchange.objects.all().filter(name=Exchange.BB).values(
            'btc_jpy_bid', 'btc_jpy_ask')[:ExchangeAdapter.SPECIMEN_NUMBER]
        cc = Exchange.objects.all().filter(name=Exchange.CC).values(
            'btc_jpy_bid', 'btc_jpy_ask')[:ExchangeAdapter.SPECIMEN_NUMBER]
        z = Exchange.objects.all().filter(name=Exchange.Z).values(
            'btc_jpy_bid', 'btc_jpy_ask')[:ExchangeAdapter.SPECIMEN_NUMBER]
        bx = Exchange.objects.all().filter(name=Exchange.BX).values(
            'btc_jpy_bid', 'btc_jpy_ask')[:ExchangeAdapter.SPECIMEN_NUMBER]
        bids = {
            Exchange.BF:
            ExchangeAdapter.running_mean(
                bf.values_list('btc_jpy_bid', flat=True))[0],
            Exchange.BB:
            ExchangeAdapter.running_mean(
                bb.values_list('btc_jpy_bid', flat=True))[0],
            Exchange.CC:
            ExchangeAdapter.running_mean(
                cc.values_list('btc_jpy_bid', flat=True))[0],
            Exchange.BX:
            ExchangeAdapter.running_mean(
                bx.values_list('btc_jpy_bid', flat=True))[0],
            Exchange.Z:
            ExchangeAdapter.running_mean(
                z.values_list('btc_jpy_bid', flat=True))[0]
        }
        asks = {
            Exchange.BF:
            ExchangeAdapter.running_mean(
                bf.values_list('btc_jpy_ask', flat=True))[0],
            Exchange.BB:
            ExchangeAdapter.running_mean(
                bb.values_list('btc_jpy_ask', flat=True))[0],
            Exchange.CC:
            ExchangeAdapter.running_mean(
                cc.values_list('btc_jpy_ask', flat=True))[0],
            Exchange.BX:
            ExchangeAdapter.running_mean(
                bx.values_list('btc_jpy_ask', flat=True))[0],
            Exchange.Z:
            ExchangeAdapter.running_mean(
                z.values_list('btc_jpy_ask', flat=True))[0]
        }
        return ExchangeAdapter.remove_current(bids, asks)

    @staticmethod
    def align():
        if Exchange.objects.all().filter(
                name=Exchange.BB).count() > ExchangeAdapter.SPECIMEN_NUMBER:
            return ExchangeAdapter.map_moving_average()
        else:
            bf = Exchange.objects.all().filter(name=Exchange.BF).order_by(
                'created_at').values().reverse().first()
            bb = Exchange.objects.all().filter(name=Exchange.BB).order_by(
                'created_at').values().reverse().first()
            bx = Exchange.objects.all().filter(name=Exchange.BX).order_by(
                'created_at').values().reverse().first()
            cc = Exchange.objects.all().filter(name=Exchange.CC).order_by(
                'created_at').values().reverse().first()
            z = Exchange.objects.all().filter(name=Exchange.Z).order_by(
                'created_at').values().reverse().first()
            bids = {
                Exchange.BF: bf['btc_jpy_bid'],
                Exchange.BB: bb['btc_jpy_bid'],
                Exchange.BX: bx['btc_jpy_bid'],
                Exchange.CC: cc['btc_jpy_bid'],
                Exchange.Z: z['btc_jpy_bid']
            }
            asks = {
                Exchange.BF: bf['btc_jpy_ask'],
                Exchange.BB: bb['btc_jpy_ask'],
                Exchange.BX: bx['btc_jpy_ask'],
                Exchange.CC: cc['btc_jpy_ask'],
                Exchange.Z: z['btc_jpy_ask']
            }
        return bids, asks

    @staticmethod
    def running_mean(x):
        cumsum = numpy.cumsum(
            numpy.insert(numpy.asarray(x, dtype=numpy.dtype(Decimal)), 0, 0))
        return (cumsum[ExchangeAdapter.SPECIMEN_NUMBER:] -
                cumsum[:-ExchangeAdapter.SPECIMEN_NUMBER]) / Decimal(
                    ExchangeAdapter.SPECIMEN_NUMBER)
