from bot.models import Exchange
from btcbox_client.sync import Client
from forex_python.converter import CurrencyRates
from bot.adapters.api_adaptee import ApiAdaptee


class Btcbox(ApiAdaptee):
    def get_address(self) -> str:
        pass

    def measure_request_amount(self):
        pass

    def get_balance(self) -> tuple:
        pass

    def withdraw(self, address: str):
        pass

    def trade_ask(self):
        pass

    def trade_bid(self):
        pass

    def collect(self):
        response = self.api.ticker()
        self.save(response.json())

    def save(self, response):
        exchange = Exchange()
        exchange.name = Exchange.BX
        exchange.btc_jpy_bid = response['sell']
        exchange.btc_jpy_ask = response['buy']
        currency = CurrencyRates()
        exchange.btc_usd_bid = currency.convert('JPY', 'USD', response['sell'])
        exchange.btc_usd_ask = currency.convert('JPY', 'USD', response['buy'])
        exchange.save()

    def __init__(self):
        super().__init__()
        self.api = Client(
            public_key='your public key', private_key='your private key')
