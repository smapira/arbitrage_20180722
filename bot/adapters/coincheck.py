import pprint

from bot.models import Exchange
from bot.adapters.api_adaptee import ApiAdaptee
import os
from forex_python.converter import CurrencyRates
from coincheck.coincheck import CoinCheck
import logging
logger = logging.getLogger('django')
"""Coincheck API Wrapper.

This module handling API.

.. _API Guide:
   https://coincheck.com/ja/documents
   https://github.com/uetchy/coincheck-python
"""


class Coincheck(ApiAdaptee):
    ADDRESS = '19ozYgtNiu91s4ZrZfjZ1517XXMDYqGZXt'
    """Coincheck Exchange class.

    Attributes:
        api (object): CoinCheck()

    """

    def get_address(self) -> str:
        return self.ADDRESS

    def __init__(self):
        super().__init__()
        self.api = CoinCheck(
            os.environ.get('COINCHECK_KEY', ''),
            os.environ.get('COINCHECK_SECRET', ''))

    def collect(self):
        if os.environ.get('COINCHECK_KEY', '') == '' or os.environ.get('COINCHECK_KEY') is None:
            return
        response = self.api.ticker.all()
        self.save(response)

    def save(self, response):
        exchange = Exchange()
        exchange.name = Exchange.CC
        exchange.btc_jpy_bid = response['bid']
        exchange.btc_jpy_ask = response['ask']
        currency = CurrencyRates()
        exchange.btc_usd_bid = currency.convert('JPY', 'USD', response['bid'])
        exchange.btc_usd_ask = currency.convert('JPY', 'USD', response['ask'])
        exchange.save()

    def get_balance(self) -> tuple:
        response = self.api.account.balance()
        if not response['success']:
            pprint.pprint(response)
            raise Exception("raise %s" % __name__)
        self.jpy = float(response['jpy'])
        self.btc = float(response['btc'])
        return self.jpy, self.btc

    def withdraw(self, address: str):
        self.measure_request_amount()
        params = {'address': address, 'amount': self.amount}
        response = self.api.send.create(params)
        if not response['success']:
            raise Exception("raise %s" % __name__)

    def trade_ask(self):
        """request trade for ask"""
        self.get_balance()
        self.measure_request_amount()
        params = {
            'rate': self.price,
            'amount': self.amount,
            'order_type': 'buy',
            'pair': 'btc_jpy'
        }
        response = self.api.order.create(params)
        if response['success'] != 1:
            raise Exception("raise %s" % __name__)

    def trade_bid(self):
        """request trade for bid"""
        self.get_balance()
        self.measure_request_amount()
        params = {
            'rate': self.price,
            'amount': self.amount,
            'order_type': 'sell',
            'pair': 'btc_jpy'
        }
        response = self.api.order.create(params)
        if response['success'] != 1:
            raise Exception("raise %s" % __name__)

    def measure_request_amount(self):
        self.measure_amount(Exchange.CC)
