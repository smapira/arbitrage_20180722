from bot.models import Exchange
from forex_python.converter import CurrencyRates
from bot.adapters.api_adaptee import ApiAdaptee


class Quoinex(ApiAdaptee):
    def get_address(self) -> str:
        pass

    def measure_request_amount(self):
        pass

    def get_balance(self) -> tuple:
        pass

    def withdraw(self, address: str):
        pass

    def trade_ask(self):
        pass

    def trade_bid(self):
        pass

    def collect(self):
        response = self.api.ticker()
        self.save(response)

    def save(self, response):
        exchange = Exchange()
        exchange.name = Exchange.CC
        exchange.btc_jpy_bid = response['bid']
        exchange.btc_jpy_ask = response['ask']
        currency = CurrencyRates()
        exchange.btc_usd_bid = currency.convert('JPY', 'USD', response['bid'])
        exchange.btc_usd_ask = currency.convert('JPY', 'USD', response['ask'])
        exchange.save()

    def __init__(self):
        super().__init__()
        # self.api = market.Market()
