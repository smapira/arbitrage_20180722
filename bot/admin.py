from django.contrib import admin
from .models import Exchange


class ExchangeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'btc_jpy_ask', 'btc_jpy_bid', 'created_at')
    list_display_links = (
        'id',
        'name',
    )


admin.site.register(Exchange, ExchangeAdmin)
