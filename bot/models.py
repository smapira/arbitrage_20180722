from django.db import models
import logging
logger = logging.getLogger('django')


class Exchange(models.Model):
    BF = 'Bitflyer'
    BB = 'Bitbank'
    BT = 'Bittrex'
    B = 'Binance'
    Z = 'Zaif'
    CC = 'Coincheck'
    BX = 'Btcbox'
    QX = 'Quoinex'
    PN = 'Poloniex'

    NAME_CHOICES = (
        (BF, 'Bitflyer'),
        (BB, 'Bitbank'),
        (BT, 'Bittrex'),
        (B, 'Binance'),
        (Z, 'Zaif'),
        (CC, 'Coincheck'),
        (BX, 'Btcbox'),
        (QX, 'Quoinex'),
        (PN, 'Poloniex'),
    )
    name = models.CharField(max_length=20, choices=NAME_CHOICES, default=BF)
    btc_jpy_ask = models.DecimalField(
        max_digits=8, decimal_places=2, default=0)
    btc_usd_ask = models.DecimalField(
        max_digits=8, decimal_places=2, default=0)
    btc_jpy_bid = models.DecimalField(
        max_digits=8, decimal_places=2, default=0)
    btc_usd_bid = models.DecimalField(
        max_digits=8, decimal_places=2, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    indexes = [
        models.Index(fields=[
            'name',
        ]),
    ]
    objects = models.Manager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-created_at']
