from bot.adapters.exchange_adapter import ExchangeAdapter
import logging
logger = logging.getLogger('django')


class Ruling:
    def __init__(self):
        self.bids, self.asks = ExchangeAdapter.align()
        self.detect = 0
        self.threshold = 10
        self.from_name = ''
        self.to_name = ''
        self.purchase_price = 0
        self.exchange_api = None

    def judge(self, from_price, to_price):
        logger.info('from_price: ' + str(from_price))
        logger.info('to_price: ' + str(to_price))
        self.detect = to_price - from_price
        logger.info('judge: ' + str(self.detect))
        if self.detect < self.threshold:
            return True
        return False
