# arbitrage bot

[![CircleCI](https://circleci.com/bb/smapira/arbitrage_20180722.svg?style=svg)](https://circleci.com/bb/smapira/arbitrage_20180722)

## Environments

- BITBANK_KEY=
- BITBANK_SECRET=
- BITFLYER_KEY=
- BITFLYER_SECRET=
- BTCBOX_KEY=
- BTCBOX_SECRET=
- FCCE_KEY=
- FCCE_SECRET=
- QUOINEX_KEY=
- QUOINEX_SECRET=
- ZAIF_KEY=
- ZAIF_SECRET=
- COINCHECK_KEY=
- COINCHECK_SECRET=

## Usage

```bash
CONTAINER=arbitrage-2018-09-16
docker run \
    -d \
    -p 8000:8000 \
    -p 8888:8888 \
    --name ${CONTAINER} \
    smapira/${CONTAINER}
```
### Jupyter
```bash
docker exec -t ${CONTAINER} jupyter notebook list
open http://0.0.0.0:8888/?token=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
```    

### Admin
```bash
open http://0.0.0.0:8000/admin
```
- user: `root`
- password: `1234qwer`

### Job
```bash
docker exec -t ${CONTAINER} pipenv run python queueing.py
```

### Debug
```bash
docker build --rm -t smapira/arbitrage-2018-09-16 .
docker exec -ti ${CONTAINER} /bin/bash
docker exec -t ${CONTAINER} tail -f log/celery.log
docker logs ${CONTAINER}
```

#### Copy docker to local
```bash
docker cp ${CONTAINER}:/home/notebooks .
cd notebooks
docker stop ${CONTAINER} && docker rm ${CONTAINER}
docker run \
    -d \
    -p 8000:8000 \
    -p 8888:8888 \
    --name ${CONTAINER} \
    -v $(pwd)/:/home/notebooks:rw \
    smapira/${CONTAINER}
```
