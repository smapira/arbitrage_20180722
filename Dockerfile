FROM smapira/docker-python-sourcecompile-deep-learning:latest
ADD . /home/notebooks
ENV PACKAGES="\
        build-essential \
        cmake \
        curl \
        libbz2-dev \
        libncurses5-dev \
        libncursesw5-dev \
        libreadline-dev \
        libssl-dev \
        linux-headers-4.9 \
        llvm \
        make \
        tcl-dev \
        tk-dev \
        wget \
        xz-utils \
        zlib1g-dev"

RUN set -ex; \
    apt-get update -y && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends ${PACKAGES};
RUN apt-get install -y sqlite3 libsqlite3-dev supervisor
RUN pip3 install --upgrade pip
RUN pip3 install pipenv
RUN pipenv install --system
RUN apt-get remove --purge --auto-remove -y ${PACKAGES}; \
    apt-get clean; \
    apt-get autoclean; \
    apt-get autoremove;
RUN cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
RUN mv supervisord.conf /etc/supervisord.conf
RUN python3 manage.py makemigrations
RUN python3 manage.py migrate
RUN jt -t chesterish
EXPOSE 8000
EXPOSE 8888
CMD []
ENTRYPOINT ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]

